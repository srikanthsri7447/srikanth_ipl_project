let matches = require('../data/matches.json');
let deliveries = require('../data/deliveries.json');


//Number of matches played per year for all the years in IPL.
function matchesPerYear(matches) {

    let iplMatchesPerYear = {}

    for (i = 0; i < matches.length; i++) {
        if (iplMatchesPerYear.hasOwnProperty(matches[i].season)) {
            iplMatchesPerYear[matches[i].season] += 1
        }
        else {
            iplMatchesPerYear[matches[i].season] = 1
        }
    }

    return iplMatchesPerYear;
}



// Number of matches won per team per year in IPL
function matchesOwnPerYear(matches) {

    let iplMatchesOwnPerYear = {}

    for (let i = 0; i < matches.length; i++) {

        if (iplMatchesOwnPerYear.hasOwnProperty(matches[i].season)) {

            if (iplMatchesOwnPerYear[matches[i].season].hasOwnProperty(matches[i].winner)) {
                iplMatchesOwnPerYear[matches[i].season][matches[i].winner] += 1;
            }
            else {
                if (matches[i].winner !== "") {
                    iplMatchesOwnPerYear[matches[i].season][matches[i].winner] = 1;
                }

            }

        }
        else {
            iplMatchesOwnPerYear[matches[i].season] = {};
        }


    }
    return iplMatchesOwnPerYear;
}


// Extra runs conceded per team in the year 2016

function extraRuns2016(deliveries, matches) {

    let idOf2016 = matches.filter((match) => match.season == 2016)
        .map((match) => parseInt(match.id));

    let extraRuns2016 = {}

    for (let i = 0; i < deliveries.length; i++) {


        if (idOf2016.includes(parseInt(deliveries[i].match_id))) {

            if (extraRuns2016.hasOwnProperty(deliveries[i].bowling_team)) {

                extraRuns2016[deliveries[i].bowling_team] += parseInt(deliveries[i].extra_runs);
            }
            else {

                extraRuns2016[deliveries[i].bowling_team] = parseInt(deliveries[i].extra_runs);
            }
        }
    }

    return extraRuns2016;


}

// Top 10 economical bowlers in the year 2015

function ecoBowler2015(deliveries, matches) {

    let idOf2015 = matches.filter((match) => match.season == 2015)
        .map((match) => parseInt(match.id))

    let economicBowlers = {}

    for (let i = 0; i < deliveries.length; i++) {

        if (idOf2015.includes(parseInt(deliveries[i].match_id))) {

            if (economicBowlers.hasOwnProperty(deliveries[i].bowler)) {

                economicBowlers[deliveries[i].bowler].balls += 1;
                economicBowlers[deliveries[i].bowler].runs += parseInt(deliveries[i].total_runs);
                let run = economicBowlers[deliveries[i].bowler].runs;
                let ball = economicBowlers[deliveries[i].bowler].balls;
                economicBowlers[deliveries[i].bowler].economy = (run / (ball / 6)).toFixed(2);
            } else {
                economicBowlers[deliveries[i].bowler] = { "runs": parseInt(deliveries[i].total_runs), "balls": 1, "economy": 0 };
            }
        }

    }

    const contiansOnlyEconomy = [];
    for (let bowlerName in economicBowlers) {
        contiansOnlyEconomy.push(economicBowlers[bowlerName].economy);
    }

    contiansOnlyEconomy.sort()


    const top10EconomyBowlers = {};
    for (let i = 0; i < 10; i++) {
        for (key in economicBowlers) {

            if (economicBowlers[key].economy == contiansOnlyEconomy[i]) {
                top10EconomyBowlers[key] = { economyRate: economicBowlers[key].economy };

            }
        }
    }

    return top10EconomyBowlers;

}








module.exports = { matchesPerYear, matchesOwnPerYear, extraRuns2016, ecoBowler2015 };
